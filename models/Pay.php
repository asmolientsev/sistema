<?php

namespace app\models;

use Yii;
use app\models\Customer;
use app\models\Operator;
use yii\helpers\ArrayHelper;
use app\models\Country;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "pay".
 *
 * @property integer $id
 * @property string $contract
 * @property string $date
 * @property integer $operator_id
 * @property integer $customer_id
 * @property double $summa
 * @property integer $count_18
 * @property integer $count_28
 * @property integer $count_other
 * @property integer $country_id
 * @property integer $count_day
 * @property double $fixed_summa
 */
class Pay extends \yii\db\ActiveRecord
{
    public function init()
    {
        parent::init();
        $this->fixed_summa = 0;        
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pay';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['contract', 'date', 'operator_id', 'customer', 'summa', 'count_18', 'count_28', 'count_other', 'country_id', 'count_day', 'fixed_summa'], 'required'],
            [['date'], 'safe'],
            [['operator_id', 'count_18', 'count_28', 'count_other', 'country_id', 'count_day'], 'integer'],
            [['summa', 'fixed_summa'], 'number'],
            [['contract', 'customer'], 'string', 'max' => 255],
            [['country.title', 'use_fixed_summa', 'operator.title'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'contract' => 'Контракт',
            'date' => 'Дата',
            'operator_id' => 'Оператор',
            'customer' => 'Фамилия туриста',
            'summa' => 'Сумма',
            'count_18' => 'Кол-во до 18',
            'count_28' => 'Кол-во до 28',
            'count_other' => 'Ко-ло остальных',
            'country_id' => 'Country ID',
            'count_day' => 'Дни',
            'fixed_summa' => 'Фиксированная сумма',
            'country.title' => 'Страна',
            'use_fixed_summa' => 'Использовать фиксированную сумму',
            'operator.title' => 'Оператор',
        ];
    }    

    public function attributes()
    {
        // add related fields to searchable attributes
        return array_merge(parent::attributes(), ['country.title', 'operator.title']);
    }

    public function getOperator(){
        return $this->hasOne(Operator::className(), ['id' => 'operator_id']);
    }

    public static function getAllItems(){
        return ArrayHelper::map(self::find()->all(), 'id', 'contract');
    }

    public function getCountry(){
        return $this->hasOne(Country::className(), ['id' => 'country_id']);
    }

    public function search($params){
        $query = self::find()->orderBy('id desc');        
        $query->innerJoinWith('country');
        $query->innerJoinWith('operator');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);     

        if (!($this->load($params) && $this->validate())) {            
            return $dataProvider;
        }   
        $query->andFilterWhere(['like', 'country.title', $this->getAttribute('country.title')]);
        $query->andFilterWhere(['like', 'operator.title', $this->getAttribute('operator.title')]);
        return $dataProvider;
    }
}