<?php

namespace app\models;

use Yii;
use app\models\Pay;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "back_pay".
 *
 * @property integer $id
 * @property integer $pay_id
 * @property double $summa_operator
 * @property double $summa_customer
 * @property double $ostatok
 */
class BackPay extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'back_pay';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pay_id', 'summa_operator', 'summa_customer', 'ostatok', 'date'], 'required'],
            [['pay_id'], 'integer'],
            [['summa_operator', 'summa_customer', 'ostatok'], 'number'],
            [['date', 'pay.contract'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'pay_id' => 'Pay ID',
            'summa_operator' => 'Сумма от оператора',
            'summa_customer' => 'Сумма туристу',
            'ostatok' => 'Остаток',
            'pay.contract' => 'Платеж',
        ];
    }

    public function attributes()
    {
        // add related fields to searchable attributes
        return array_merge(parent::attributes(), ['pay.contract']);
    }

    public function getPay(){
        return $this->hasOne(Pay::className(), ['id'=>'pay_id']);
    }

    public function search($params){
        $query = self::find()->orderBy('id desc');        
        $query->innerJoinWith('pay');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);     

        if (!($this->load($params) && $this->validate())) {            
            return $dataProvider;
        }   
        $query->andFilterWhere(['like', 'pay.contract', $this->getAttribute('pay.contract')]);
        return $dataProvider;
    }
}