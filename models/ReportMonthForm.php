<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 */
class ReportMonthForm extends Model
{
    public $month;
    public $year;
    
    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['month', 'year'], 'required'],                    
        ];
    }

    public function init(){
        $this->year = date('Y');        
    }

    public function attributeLabels()
    {
        return [
            'month' => 'Месяц',
            'year' => 'Год',
        ];
    }
}
