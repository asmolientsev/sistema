<?php

namespace app\models;

use Yii;
use app\models\Pay;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "add_pay".
 *
 * @property integer $id
 * @property integer $pay_id
 * @property double $summa
 * @property integer $count_18
 * @property integer $count_28
 * @property integer $count_other
 * @property integer $count_day
 */
class AddPay extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'add_pay';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pay_id', 'summa', 'count_18', 'count_28', 'count_other', 'count_day', 'date'], 'required'],
            [['pay_id', 'count_18', 'count_28', 'count_other', 'count_day'], 'integer'],
            [['summa', 'fixed_summa'], 'number'],
            [['date', 'use_fixed_summa', 'pay.contract'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'pay_id' => 'Платеж',
            'summa' => 'Сумма',
            'count_18' => 'Кол-во до 18',
            'count_28' => 'Кол-во до 28',
            'count_other' => 'Ко-ло остальных',
            'count_day' => 'Дни',
            'fixed_summa' => 'Фиксированная сумма',
            'date' => 'Дата',
            'pay.contract' => 'Платеж',
        ];
    }

    public function attributes()
    {
        // add related fields to searchable attributes
        return array_merge(parent::attributes(), ['pay.contract']);
    }

    public function getPay(){
        return $this->hasOne(Pay::className(), ['id'=>'pay_id']);
    }

    public function search($params){
        $query = self::find()->orderBy('id desc');        
        $query->innerJoinWith('pay');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);     

        if (!($this->load($params) && $this->validate())) {            
            return $dataProvider;
        }   
        $query->andFilterWhere(['like', 'pay.contract', $this->getAttribute('pay.contract')]);
        return $dataProvider;
    }
}