-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Янв 09 2015 г., 21:55
-- Версия сервера: 5.5.40-0ubuntu0.14.04.1
-- Версия PHP: 5.5.9-1ubuntu4.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `sistema`
--

-- --------------------------------------------------------

--
-- Структура таблицы `add_pay`
--

CREATE TABLE IF NOT EXISTS `add_pay` (
`id` int(11) NOT NULL,
  `pay_id` int(11) NOT NULL,
  `summa` float NOT NULL,
  `count_18` int(11) NOT NULL,
  `count_28` int(11) NOT NULL,
  `count_other` int(11) NOT NULL,
  `count_day` int(11) NOT NULL,
  `date` date NOT NULL,
  `fixed_summa` float NOT NULL,
  `use_fixed_summa` tinyint(1) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `add_pay`
--

INSERT INTO `add_pay` (`id`, `pay_id`, `summa`, `count_18`, `count_28`, `count_other`, `count_day`, `date`, `fixed_summa`, `use_fixed_summa`) VALUES
(1, 1, 5000, 2, 2, 0, 1, '2015-01-02', 0, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `back_pay`
--

CREATE TABLE IF NOT EXISTS `back_pay` (
`id` int(11) NOT NULL,
  `pay_id` int(11) NOT NULL,
  `summa_operator` float NOT NULL,
  `summa_customer` float NOT NULL,
  `ostatok` float NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `back_pay`
--

INSERT INTO `back_pay` (`id`, `pay_id`, `summa_operator`, `summa_customer`, `ostatok`, `date`) VALUES
(1, 1, 2000, 2000, 0, '2015-01-07');

-- --------------------------------------------------------

--
-- Структура таблицы `country`
--

CREATE TABLE IF NOT EXISTS `country` (
`id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `title_de` varchar(255) DEFAULT NULL,
  `title_en` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=247 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `country`
--

INSERT INTO `country` (`id`, `title`, `title_de`, `title_en`) VALUES
(2, 'Австралия', 'Australien', 'Australia'),
(3, 'Австрия', 'Österreich', 'Austria'),
(4, 'Азербайджан', 'Aserbaidschan', 'Azerbaijan'),
(5, 'Аландские острова', 'Ascension Island', 'Aland Islands'),
(6, 'Албания', 'Albanien', 'Albania'),
(7, 'Алжир', 'Algerien', 'Algeria'),
(8, 'Ангилья', 'Anguilla', 'Anguilla'),
(9, 'Ангола', 'Angola', 'Angola'),
(10, 'Андорра', 'Andorra', 'Andorra'),
(11, 'Антарктика', 'Antarktis', 'Antarctica'),
(12, 'Антигуа и Барбуда', 'Antigua und Barbuda', 'Antigua and Barbuda'),
(13, 'Антильские острова (Нид.)', 'Niederländische Antillen', 'Netherlands Antilles'),
(14, 'Аомынь (Макао)', 'Macao', 'Macau'),
(15, 'Аргентина', 'Argentinien', 'Argentina'),
(16, 'Армения', 'Armenien', 'Armenia'),
(17, 'Аруба (Антильские острова Нид.)', 'Aruba', 'Aruba'),
(18, 'Афганистан', 'Afghanistan', 'Afghanistan'),
(19, 'Багамские Острова', 'Bahamas', 'Bahamas'),
(20, 'Бангладеш', 'Bangladesch', 'Bangladesh'),
(21, 'Барбадос', 'Barbados', 'Barbados'),
(22, 'Бахрейн', 'Bahrain', 'Bahrain'),
(23, 'Белиз', 'Belize', 'Belize'),
(24, 'Белоруссия', 'Weißrußland', 'Belarus'),
(25, 'Бельгия', 'Belgien', 'Belgium'),
(26, 'Бенин', 'Benin', 'Benin'),
(27, 'Бермудские Острова', 'Bermuda', 'Bermuda'),
(28, 'Болгария', 'Bulgarien', 'Bulgaria'),
(29, 'Боливия', 'Bolivien', 'Bolivia'),
(30, 'Босния и Герцеговина', 'Bosnien-Herzegowina', 'Bosnia and Herzegovina'),
(31, 'Ботсвана', 'Botswana', 'Botswana'),
(32, 'Бразилия', 'Brasilien', 'Brazil'),
(33, 'Британские территории в Индийском океане', 'Britische Territorien im Indischen Ozean', 'British Indian Ocean Territory'),
(34, 'Бруней', 'Brunei', 'Brunei'),
(35, 'Буве остров', 'Bouvet-Insel', 'Bouvet Island'),
(36, 'Буркина-Фасо', 'Burkina Faso', 'Burkina Faso'),
(37, 'Бурунди', 'Burundi', 'Burundi'),
(38, 'Бутан', 'Bhutan', 'Bhutan'),
(39, 'Вануату', 'Vanuatu', 'Vanuatu'),
(40, 'Ватикан', 'Vatikanstadt', 'Vatican (or Holy See where id='),
(41, 'Великобритания', 'Großbritannien', 'Great Britain'),
(42, 'Венгрия', 'Ungarn', 'Hungary'),
(43, 'Венесуэла', 'Venezuela', 'Venezuela'),
(44, 'Виргинские острова (Брит.)', 'Jungferninseln (Britisch)', 'Virgin Islands= British'),
(45, 'Виргинские острова (США)', 'Jungferninseln (U.S.)', 'Virgin Islands= U.S.'),
(46, 'Восточное Самоа', 'Amerikanisch Samoa', 'American Samoa'),
(47, 'Восточный Тимор', 'Ost Timor', 'Timor-Leste (East Timor)'),
(48, 'Вьетнам', 'Vietnam', 'Vietnam'),
(49, 'Габон', 'Gabun', 'Gabon'),
(50, 'Гаити', 'Haïti', 'Haiti'),
(51, 'Гайана', 'Guyana', 'Guyana'),
(52, 'Гамбия', 'Gambia', 'Gambia'),
(53, 'Гана', 'Ghana', 'Ghana'),
(54, 'Гваделупа', 'Guadeloupe', 'Guadeloupe'),
(55, 'Гватемала', 'Guatemala', 'Guatemala'),
(56, 'Гвиана (Фр.)', 'Guyana', 'Guyana'),
(57, 'Гвинея', 'Guinea', 'Guinea'),
(58, 'Гвинея-Бисау', 'Guinea-Bissau', 'Guinea-Bissau'),
(59, 'Германия', 'Deutschland', 'Germany'),
(60, 'Гернси', 'Guernsey', 'Guernsey'),
(61, 'Гибралтар', 'Gibraltar', 'Gibraltar'),
(62, 'Гондурас', 'Honduras', 'Honduras'),
(63, 'Гренада', 'Grenada', 'Grenada'),
(64, 'Гренландия', 'Grönland', 'Greenland'),
(65, 'Греция', 'Griechenland', 'Greece'),
(66, 'Грузия', 'Georgien', 'Georgia'),
(67, 'Гуам', 'Guam', 'Guam'),
(68, 'Дания', 'Dänemark', 'Denmark'),
(69, 'Джерси', 'Jersey', 'Jersey'),
(70, 'Джибути', 'Dschibuti', 'Djibouti'),
(71, 'Доминика', 'Dominica', 'Dominica'),
(72, 'Доминиканская Республика', 'Dominikanische Republik', 'Dominican Republic'),
(73, 'Египет', 'Ägypten', 'Egypt'),
(74, 'Замбия', 'Sambia', 'Zambia'),
(75, 'Западная Сахара', 'Westsahara', 'Western Sahara'),
(76, 'Западное Самоа', 'Samoa', 'Samoa'),
(77, 'Зимбабве', 'Zimbabwe', 'Zimbabwe'),
(78, 'Израиль', 'Israel', 'Israel'),
(79, 'Индия', 'Indien', 'India'),
(80, 'Индонезия', 'Indonesien', 'Indonesia'),
(81, 'Иордания', 'Jordanien', 'Jordan'),
(82, 'Ирак', 'Irak', 'Iraq'),
(83, 'Иран', 'Iran', 'Iran'),
(84, 'Ирландия', 'Irland', 'Ireland'),
(85, 'Исландия', 'Island', 'Iceland'),
(86, 'Испания', 'Spanien', 'Spain'),
(87, 'Италия', 'Italien', 'Italy'),
(88, 'Йемен', 'Jemen', 'Yemen'),
(89, 'Кабо-Верде', 'Kap Verde', 'Cape Verde'),
(90, 'Казахстан', 'Kasachstan', 'Kazakhstan'),
(91, 'Кайман острова', 'Cayman-Inseln', 'Cayman Islands'),
(92, 'Камбоджа', 'Kambodscha', NULL),
(93, 'Камерун', 'Kamerun', 'Cambodia'),
(94, 'Канада', 'Kanada', 'Canada'),
(95, 'Катар', 'Katar', 'Qatar'),
(96, 'Кения', 'Kenia', 'Kenya'),
(97, 'Кипр', 'Zypern', 'Cyprus'),
(98, 'Киргизия', 'Kirgisistan', 'Kyrgyzstan'),
(99, 'Кирибати', 'Kiribati', 'Kiribati'),
(100, 'Китай', 'China', 'China'),
(101, 'Кокосовые(Килинг) острова', 'Kokosinseln (Keeling)', 'Cocos Islands (Keeling)'),
(102, 'Колумбия', 'Kolumbien', 'Colombia'),
(103, 'Коморские Острова', 'Komoren', 'Comoros'),
(104, 'Конго', 'Congo', 'Congo'),
(105, 'Конго, Демократическая Республика', 'Kongo= Demokratische Republik', 'Congo= the Democratic Republic of the'),
(106, 'Коста-Рика', 'Costa Rica', 'Costa Rica'),
(107, 'Кот-д''Ивуар', 'Côte d''Ivoire', 'Côte d''Ivoire'),
(108, 'Куба', 'Kuba', 'Cuba'),
(109, 'Кувейт', 'Kuwait', 'Kuwait'),
(110, 'Кука Острова', 'Cookinseln', 'Cook Islands'),
(111, 'Лаос', 'Laos', 'Laos'),
(112, 'Латвия', 'Lettland', 'Latvia'),
(113, 'Лесото', 'Lesotho', 'Lesotho'),
(114, 'Либерия', 'Liberia', 'Liberia'),
(115, 'Ливан', 'Libanon', 'Lebanon'),
(116, 'Ливия', 'Libyen', 'Libya'),
(117, 'Литва', 'Litauen', 'Lithuania'),
(118, 'Лихтенштейн', 'Liechtenstein', 'Liechtenstein'),
(119, 'Люксембург', 'Luxemburg', 'Luxembourg'),
(120, 'Маврикий', 'Mauritius', 'Mauritius'),
(121, 'Мавритания', 'Mauretanien', 'Mauritania'),
(122, 'Мадагаскар', 'Madagaskar', 'Madagascar'),
(123, 'Майотта', 'Mayotte', 'Mayotte'),
(124, 'Македония', 'Mazedonien', 'Macedonia'),
(125, 'Малави', 'Malawi', 'Malawi'),
(126, 'Малайзия', 'Malaysia', 'Malaysia'),
(127, 'Мали', 'Mali', 'Mali'),
(128, 'Мальдивы', 'Malediven', 'Maldives'),
(129, 'Мальдивы', 'Malta', 'Malta'),
(130, 'Марокко', 'Marokko', 'Morocco'),
(131, 'Мартиника', 'Martinique', 'Martinique'),
(132, 'Маршалловы Острова', 'Marshall-Inseln', 'Marshall Islands'),
(133, 'Мексика', 'Mexiko', 'Mexico'),
(134, 'Мелкие отдаленные острова США', 'Kleinere Amerikanische Überseeinseln', 'United States Minor Outlying Islands'),
(135, 'Микронезия, Федеративные Штаты', 'Mikronesien', 'Micronesia= Federated States of'),
(136, 'Мозамбик', 'Mosambik', 'Mozambique'),
(137, 'Молдавия', 'Moldawien', 'Moldova'),
(138, 'Монако', 'Monaco', 'Monaco'),
(139, 'Монголия', 'Mongolei', 'Mongolia'),
(140, 'Монтсеррат', 'Montserrat', 'Montserrat'),
(141, 'Мьянма', 'Myanmar', 'Myanmar'),
(142, 'Намибия', 'Namibia', 'Namibia'),
(143, 'Науру', 'Nauru', 'Nauru'),
(144, 'Непал', 'Nepal', 'Nepal'),
(145, 'Нигер', 'Niger', 'Niger'),
(146, 'Нигерия', 'Nigeria', 'Nigeria'),
(147, 'Нидерланды', 'Niederlande', 'Netherlands'),
(148, 'Никарагуа', 'Nicaragua', 'Nicaragua'),
(149, 'Ниуэ', 'Niue', 'Niue'),
(150, 'Новая Зеландия', 'Neuseeland', 'New Zealand'),
(151, 'Новая Каледония', 'Neu-Kaledonien', 'New Caledonia'),
(152, 'Норвегия', 'Norwegen', 'Norway'),
(153, 'Норфолк', 'Norfolk-Insel', 'Norfolk Island'),
(154, 'Объединенные Арабские Эмираты', 'Vereinigte Arabische Emirate', 'United Arab Emirates'),
(155, 'Оман', 'Oman', 'Oman'),
(156, 'Остров Мэн', 'Isle of Man', 'Isle of Man'),
(157, 'Пакистан', 'Pakistan', 'Pakistan'),
(158, 'Палау', 'Palau', 'Palau'),
(159, 'Палестина', 'Palästina', 'Palestine'),
(160, 'Панама', 'Panama', 'Panama'),
(161, 'Папуа-Новая Гвинея', 'Papua-Neuguinea', 'Papua New Guinea'),
(162, 'Парагвай', 'Paraguay', 'Paraguay'),
(163, 'Перу', 'Peru', 'Peru'),
(164, 'Питкэрн', 'Pitcairn', 'Pitcairn'),
(165, 'Польша', 'Polen', 'Poland'),
(166, 'Португалия', 'Portugal', 'Portugal'),
(167, 'Пуэрто-Рико', 'Puerto Rico', 'Puerto Rico'),
(168, 'Реюньон', 'Réunion', 'Reunion'),
(169, 'Рождества остров', 'Weihnachtsinseln', 'Christmas Island'),
(170, 'Россия', 'Russland', 'Russia'),
(171, 'Руанда', 'Ruanda', 'Rwanda'),
(172, 'Румыния', 'Rumänien', 'Romania'),
(173, 'Сальвадор', 'El Salvador', 'El Salvador'),
(174, 'Сан-Марино', 'San Marino', 'San Marino'),
(175, 'Сан-Томе и Принсипи', 'São Tomé und Príncipe', 'Sao Tome and Principe'),
(176, 'Саудовская Аравия', 'Saudiarabien', 'Saudi Arabia'),
(177, 'Свазиленд', 'Swasiland', 'Swaziland'),
(178, 'Святой Елены остров', 'Sankt Helena', 'Saint Helena'),
(179, 'Северная Корея', 'Nordkorea', 'North Korea'),
(180, 'Северные Марианские острова', 'Nördliche Marianen Inseln', 'Northern Mariana Islands'),
(181, 'Сейшельские острова', 'Seychellen', 'Seychelles'),
(182, 'Сенегал', 'Senegal', 'Senegal'),
(183, 'Сен-Пьер и Микелон', 'Saint Pierre und Miquelon', 'Saint Pierre and Miquelon'),
(184, 'Сент Винсент и Гренадины', 'Sankt Vincent und die Grenadinen', 'Saint Vincent and the Grenadines'),
(185, 'Сент-Китс и Невис', 'St. Christopher und Nevis', 'Saint Kitts and Nevis'),
(186, 'Сент-Люсия', 'Saint Lucia', 'Saint Lucia'),
(187, 'Сербия и Черногория', 'Serbien und Montenegro', 'Serbia and Montenegro'),
(188, 'Сингапур', 'Singapur', 'Singapore'),
(189, 'Сирия', 'Syrien', 'Syria'),
(190, 'Словакия', 'Slowakei', 'Slovakia'),
(191, 'Словения', 'Slowenien', 'Slovenia'),
(192, 'Соединенные Штаты Америки', 'Vereinigte Staaten', 'United States'),
(193, 'Соединённое Королевство', 'Vereinigtes Königreich', 'United Kingdom'),
(194, 'Соломоновы Острова', 'Salomonen', 'Solomon Islands'),
(195, 'Сомали', 'Somalia', 'Somalia'),
(196, 'Судан', 'Sudan', 'Sudan'),
(197, 'Суринам', 'Surinam', 'Suriname'),
(198, 'Сьерра-Леоне', 'Sierra Leone', 'Sierra Leone'),
(199, 'Сянган (Гонконг)', 'Hongkong', 'Hong Kong'),
(200, 'Таджикистан', 'Tadschikistan', 'Tajikistan'),
(201, 'Таиланд', 'Thailand', 'Thailand'),
(202, 'Тайвань', 'Taiwan', 'Taiwan'),
(203, 'Танзания', 'Tansania', 'Tanzania'),
(204, 'Теркс и Кайкос острова', 'Turks- und Caicosinseln', 'Turks and Caicos Islands'),
(205, 'Того', 'Togo', 'Togo'),
(206, 'Токелау', 'Tokelau', 'Tokelau'),
(207, 'Тонга', 'Tonga', 'Tonga'),
(208, 'Тринидад и Тобаго', 'Trinidad und Tobago', 'Trinidad and Tobago'),
(209, 'Тувалу', 'Tuvalu', 'Tuvalu'),
(210, 'Тунис', 'Tunisien', 'Tunisia'),
(211, 'Туркмения', 'Turkmenistan', 'Turkmenistan'),
(212, 'Турция', 'Türkei', 'Turkey'),
(213, 'Уганда', 'Uganda', 'Uganda'),
(214, 'Узбекистан', 'Usbekistan', 'Uzbekistan'),
(215, 'Украина', 'Ukraine', 'Ukraine'),
(216, 'Уолис и Футуна острова', 'Wallis und Futuna', 'Wallis and Futuna'),
(217, 'Уругвая', 'Uruguay', 'Uruguay'),
(218, 'Фарерские Острова', 'Färöer Inseln', 'Faroe Islands'),
(219, 'Фиджи', 'Fidschi', 'Fiji'),
(220, 'Филиппины', 'Philippinen', 'Philippines'),
(221, 'Финляндия', 'Finnland', 'Finland'),
(222, 'Фолклендские (Мальвинские) Острова', 'Falklandinseln', 'Falkland Islands'),
(223, 'Франция', 'Frankreich', 'France'),
(224, 'Франция, Метрополия', 'Frankreich, das Mutterland', 'France, Mother country'),
(225, 'Французская Полинезия', 'Französisch Polynesien', 'French Polynesia'),
(226, 'Французские Южные Территории', 'Französische Südgebiete', 'French Southern Territories'),
(227, 'Херд и Макдональд, острова', 'Heard- und Mcdonald-Inseln', 'Heard Island and McDonald Islands'),
(228, 'Хорватия', 'Kroatien', 'Croatia'),
(229, 'Центральноафриканская республика', 'Zentralafrikanische Republik', 'Central African Republic'),
(230, 'Чад', 'Tschad', 'Chad'),
(231, 'Чехия', 'Tschechien', 'Czechia'),
(232, 'Чили', 'Chile', 'Chile'),
(233, 'Швейцария', 'Schweiz', 'Switzerland'),
(234, 'Швеция', 'Schweden', 'Sweden'),
(235, 'Шпицберген и Ян-Майен', 'Svalbard und Jan Mayen', 'Svalbard and Jan Mayen'),
(236, 'Шри-Ланка', 'Sri Lanka', 'Sri Lanka'),
(237, 'Эквадор', 'Ecuador', 'Ecuador'),
(238, 'Экваториальная Гвинея', 'Äquatorial-Guinea', 'Equatorial Guinea'),
(239, 'Эритрея', 'Eritrea', 'Eritrea'),
(240, 'Эстония', 'Estland', 'Estonia'),
(241, 'Эфиопия', 'Äthiopien', 'Ethiopia'),
(242, 'Южная Африка', 'Südafrika', 'South Africa'),
(243, 'Южная Георгия и Южные Сандвичевы Острова', 'Südgeorgien und Sandwich-Inseln', 'South Georgia and the South Sandwich Islands'),
(244, 'Южная Корея', 'Südkorea', 'South Korea'),
(245, 'Ямайка', 'Jamaika', 'Jamaica'),
(246, 'Япония', 'Japan', 'Japan');

-- --------------------------------------------------------

--
-- Структура таблицы `customer`
--

CREATE TABLE IF NOT EXISTS `customer` (
`id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `operator`
--

CREATE TABLE IF NOT EXISTS `operator` (
`id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `pay`
--

CREATE TABLE IF NOT EXISTS `pay` (
`id` int(11) NOT NULL,
  `contract` varchar(255) NOT NULL,
  `date` date NOT NULL,
  `operator_id` int(11) NOT NULL,
  `customer` varchar(255) NOT NULL,
  `summa` float NOT NULL,
  `count_18` int(11) NOT NULL,
  `count_28` int(11) NOT NULL,
  `count_other` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  `count_day` int(11) NOT NULL,
  `fixed_summa` float NOT NULL,
  `use_fixed_summa` tinyint(1) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `pay`
--

INSERT INTO `pay` (`id`, `contract`, `date`, `operator_id`, `customer`, `summa`, `count_18`, `count_28`, `count_other`, `country_id`, `count_day`, `fixed_summa`, `use_fixed_summa`) VALUES
(1, '1111', '2014-12-23', 1, 'Customer1', 10000, 2, 3, 0, 2, 5, 0, 0),
(2, '1112', '2015-01-06', 1, 'Customer1', 22000, 2, 3, 0, 5, 5, 0, 0);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `add_pay`
--
ALTER TABLE `add_pay`
 ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `back_pay`
--
ALTER TABLE `back_pay`
 ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `country`
--
ALTER TABLE `country`
 ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `customer`
--
ALTER TABLE `customer`
 ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `operator`
--
ALTER TABLE `operator`
 ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `pay`
--
ALTER TABLE `pay`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `add_pay`
--
ALTER TABLE `add_pay`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблицы `back_pay`
--
ALTER TABLE `back_pay`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблицы `country`
--
ALTER TABLE `country`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=247;
--
-- AUTO_INCREMENT для таблицы `customer`
--
ALTER TABLE `customer`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `operator`
--
ALTER TABLE `operator`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `pay`
--
ALTER TABLE `pay`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
