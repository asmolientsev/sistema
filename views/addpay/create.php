<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\AddPay */

$this->title = 'Создать новый дополнительный платеж';
$this->params['breadcrumbs'][] = ['label' => 'Доп платежи', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="add-pay-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>