<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Pay; 
use yii\jui\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Pay */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="add-pay-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="left w45 pr20">
        <?= $form->field($model, 'pay_id')->dropDownList(Pay::getAllItems()); ?>

        <div class="form-group field-addpay-date">
            <label class="control-label" for="addpay-date">Дата</label><br/>
            <?= DatePicker::widget([
                    'name'  => 'AddPay[date]',
                    'value'  => $model->date ? $model->date : time(),
                    'id'=>"addpay-date",   
                    'dateFormat' => 'yyyy-MM-dd',                      
                ]); ?>
            <div class="help-block"></div>
        </div>

        <?= $form->field($model, 'summa')->textInput() ?>

        <?= $form->field($model, 'count_18')->textInput() ?>

        <?= $form->field($model, 'count_28')->textInput() ?>

        <?= $form->field($model, 'count_other')->textInput() ?>
    </div>
    <div class="left w45">
        <?= $form->field($model, 'count_day')->textInput() ?>

        <?= $form->field($model, 'fixed_summa')->textInput() ?>

        <?= $form->field($model, 'use_fixed_summa')->checkBox() ?>
    </div>
    <div class="clear"></div>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Отправить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
