 <?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\AddPay */

$this->title = 'Редактировать дополнительный платеж: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Доп платежи', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактировать';
?>
<div class="add-pay-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
