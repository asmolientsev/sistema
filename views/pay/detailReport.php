<?php
	use yii\grid\GridView;

    $this->title = 'Детальный отчет';
    $this->params['breadcrumbs'][] = ['label' => 'Платежи', 'url' => ['index']];
    $this->params['breadcrumbs'][] = $this->title;
?>

<?= $this->render('_monthReportForm', ['model' => $model]); ?>
	<hr/>
<?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],         
            'contract',
            'date',
            'customer',
            'operator',
            'summa',
            'summa_operator',
            'ostatok',
            'type',
            // 'count_other',
            // 'count_day',

            // ['class' => 'yii\grid\ActionColumn'],
        ],
    ]);	
?>