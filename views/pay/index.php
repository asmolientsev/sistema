<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Платежы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pay-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Создать платеж', ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Создать доп платеж', ['/addpay/create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Создать возврат', ['/backpay/create'], ['class' => 'btn btn-success']) ?>
    </p>
    <p>
    <?= Html::a('Отчет за месяц', ['monthreport'], ['class' => 'btn btn-primary']) ?>
    <?= Html::a('Детальный отчет', ['detailreport'], ['class' => 'btn btn-primary']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $model,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'contract',
            'date',
            'operator.title',
            'customer',
            'summa',
            // 'count_18',
            // 'count_28',
            // 'count_other',
            'country.title',
            'count_day',
            // 'fixed_summa',

            [
                'class' => 'yii\grid\ActionColumn',
                'headerOptions' => ['width'=>'7%'],
            ],
        ],
    ]); ?>

</div>