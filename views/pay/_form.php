<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\jui\DatePicker;
use app\models\Country;
use app\models\Operator;

/* @var $this yii\web\View */
/* @var $model app\models\Pay */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pay-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="left w45 pr20">
        <?= $form->field($model, 'contract')->textInput(['maxlength' => 255]) ?>

        <?php //$form->field($model, 'date')->textInput() ?>
        <div class="form-group field-pay-date">
            <label class="control-label" for="pay-date">Дата</label><br/>
            <?= DatePicker::widget([
                    'name'  => 'Pay[date]',
                    'value'  => $model->date ? $model->date : time(),
                    'id'=>"pay-date",   
                    'dateFormat' => 'yyyy-MM-dd',                           
                ]); ?>
            <div class="help-block"></div>
        </div>

        <?= $form->field($model, 'operator_id')->dropDownList(Operator::getAllItems()) ?>

        <?= $form->field($model, 'customer')->textInput() ?>

        <?= $form->field($model, 'summa')->textInput() ?>

        <?= $form->field($model, 'count_18')->textInput() ?>
    </div>
    <div class="left w45">
        <?= $form->field($model, 'count_28')->textInput() ?>

        <?= $form->field($model, 'count_other')->textInput() ?>

        <?= $form->field($model, 'country_id')->dropDownList(Country::getAllItems()); ?>

        <?= $form->field($model, 'count_day')->textInput() ?>

        <?= $form->field($model, 'fixed_summa')->textInput() ?>

        <?= $form->field($model, 'use_fixed_summa')->checkBox() ?>
    </div>
    <div class="clear"></div>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Отправить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>