<?php
	use yii\grid\GridView;

    $this->title = 'Отчет за месяц';
    $this->params['breadcrumbs'][] = ['label' => 'Платежи', 'url' => ['index']];
    $this->params['breadcrumbs'][] = $this->title;
?>

<?= $this->render('_monthReportForm', ['model' => $model]); ?>
	<hr/>
<?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],                  
            'date',
            'summa',
            'summa_operator',
            'ostatok',
            // 'count_other',
            // 'count_day',

            // ['class' => 'yii\grid\ActionColumn'],
        ],
    ]);	
?>