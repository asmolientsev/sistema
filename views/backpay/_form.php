<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Pay;
use yii\jui\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\BackPay */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="back-pay-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="w45">
	    <?= $form->field($model, 'pay_id')->dropDownList(Pay::getAllItems()); ?>

		<div class="form-group field-backpay-date">
            <label class="control-label" for="backpay-date">Дата</label><br/>
            <?= DatePicker::widget([
                    'name'  => 'BackPay[date]',
                    'value'  => $model->date ? $model->date : time(),
                    'id'=>"backpay-date",   
                    'dateFormat' => 'yyyy-MM-dd',                           
                ]); ?>
            <div class="help-block"></div>
        </div>

	    <?= $form->field($model, 'summa_operator')->textInput() ?>

	    <?= $form->field($model, 'summa_customer')->textInput() ?>

	    <?= $form->field($model, 'ostatok')->textInput() ?>
	</div>
	<div class="clear"></div>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Отправить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>