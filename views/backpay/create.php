<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\BackPay */

$this->title = 'Создать возврат';
$this->params['breadcrumbs'][] = ['label' => 'Возвраты', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="back-pay-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>