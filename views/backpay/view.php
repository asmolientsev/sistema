<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\BackPay */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Возвраты', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="back-pay-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены что хотите удалить возврат?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'pay_id',
            'date',
            'summa_operator',
            'summa_customer',
            'ostatok',
        ],
    ]) ?>

</div>