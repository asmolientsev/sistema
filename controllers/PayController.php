<?php

namespace app\controllers;

use Yii;
use app\models\Pay;
use app\models\AddPay;
use app\models\BackPay;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\ReportMonthForm;
use yii\data\ArrayDataProvider;
use yii\db\Query;

/**
 * PayController implements the CRUD actions for Pay model.
 */
class PayController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Pay models.
     * @return mixed
     */
    public function actionIndex()
    {
        $model = new Pay();
        
        $dataProvider = $model->search(Yii::$app->request->get());

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'model' => $model,
        ]);
    }

    /**
     * Displays a single Pay model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Pay model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Pay();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Pay model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Pay model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Pay model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Pay the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Pay::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionMonthreport(){
        $allModels = [];
        $model = new ReportMonthForm();      
        if ($model->load(Yii::$app->request->post()) && $model->validate()){            
            $month = $model->month+1;
            if ($model->month<10) $model->month = '0'.$model->month;
            if ($month<10) $month = '0'.$month;
            $query = ['and'];
            $query[] = ['>', 'date', $model->year.'-'.$model->month.'-00']; 
            if ($model->month==12){
                $query[] = ['<', 'date', ($model->year+1).'-01-01'];                 
            }else{
                $query[] = ['<', 'date', $model->year.'-'.$month.'-00'];                 
            }                      
            $pays = Pay::find()
                ->where($query)
                ->orderBy('date')
                ->all();                

            $date = '';
            $summa = 0;
            $summa_operator = 0;
            $ostatok = 0;
            if (count($pays)>0) $date = $pays[0]->date;
            foreach ($pays as $pay) {
                if ($pay->date!=$date){
                    $allModels[] = [
                        'date' => $date,
                        'summa' => $summa,
                        'summa_operator' => $summa_operator,
                        'ostatok' => $ostatok,
                    ];
                    $date = $pay->date;                    
                    $summa_operator = $pay->summa;
                    if ($pay->fixed_summa>0)
                        $_summa = $pay->fixed_summa;
                    else
                        $_summa = $this->getGeneralSumma($pay->summa);
                    $ostatok = $_summa;
                    $summa = $_summa+$summa_operator;              
                }else{                    
                    $summa_operator += $pay->summa;
                    if ($pay->fixed_summa>0)
                        $_summa = $pay->fixed_summa;
                    else
                        $_summa = $this->getGeneralSumma($pay->summa);
                    $ostatok += $_summa;
                    $summa += $_summa+$summa_operator;
                }                
            }                            
            if ($date!=''){
                    $allModels[] = [
                        'date' => $date,
                        'summa' => $summa,
                        'summa_operator' => $summa_operator,
                        'ostatok' => $ostatok,
                    ];
                }  

            $pays = AddPay::find()
                ->where($query)
                ->orderBy('date')
                ->all();                
            $date = '';
            $summa = 0;
            $summa_operator = 0;
            $ostatok = 0;
            if (count($pays)>0) $date = $pays[0]->date;
            foreach ($pays as $pay) {
                if ($pay->date!=$date){
                    $allModels[] = [
                        'date' => $date,
                        'summa' => $summa,
                        'summa_operator' => $summa_operator,
                        'ostatok' => $ostatok,
                    ];
                    $date = $pay->date;                    
                    $summa_operator = $pay->summa;
                    if ($pay->use_fixed_summa)
                        $_summa = $pay->fixed_summa;
                    else
                        $_summa = $this->getGeneralSumma($pay->summa);
                    $ostatok = $_summa;
                    $summa = $_summa+$summa_operator;          
                }else{                    
                    $summa_operator += $pay->summa;
                    $_summa = $pay->fixed_summa;                    
                    $ostatok += $_summa;
                    $summa += $_summa+$summa_operator;
                }                
            }            
            if ($date!=''){
                    $allModels[] = [
                        'date' => $date,
                        'summa' => $summa,
                        'summa_operator' => $summa_operator,
                        'ostatok' => $ostatok,
                    ];
                }  

            $pays = BackPay::find()
                ->where($query)
                ->orderBy('date')
                ->all();                
            $date = '';
            $summa = 0;
            $summa_operator = 0;
            $ostatok = 0;
            if (count($pays)>0) $date = $pays[0]->date;
            foreach ($pays as $pay) {
                if ($pay->date!=$date){
                    $allModels[] = [
                        'date' => $date,
                        'summa' => $summa,
                        'summa_operator' => $summa_operator,
                        'ostatok' => $ostatok,
                    ];
                    $date = $pay->date;                    
                    $summa_operator = $pay->summa_operator;                   
                    $summa = $summa_operator;              
                }else{                    
                    $summa_operator += $pay->summa_operator;                    
                    $summa += $summa_operator;
                }                
            }                            
            if ($date!=''){
                    $allModels[] = [
                        'date' => $date,
                        'summa' => $summa,
                        'summa_operator' => $summa_operator,
                        'ostatok' => $ostatok,
                    ];                   
                }  

            usort($allModels, function ($val1, $val2){
                    if ($val1['date']<$val2['date']) return -1;
                    elseif ($val1['date']>$val2['date']) return 1;
                    else return 0;
            });            
            $allModels = $this->dateTogether($allModels);
        }
        $query = new Query();
        $dataProvider = new ArrayDataProvider([
            'allModels' => $allModels,
            // 'sort' => [
            //     'attributes' => ['id', 'username', 'email'],
            // ],
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);
        return $this->render('monthReport', ['model' => $model, 'dataProvider' => $dataProvider]);            
    }

    private function getGeneralSumma($summa){        
        if ($summa>1 && $summa<10000)
            return 200;
        elseif ($summa>=10000 && $summa<20000)
            return 500;
        elseif ($summa>=20000)
            return 500;
        else
            return 0;
    }    

    private function dateTogether($in){
        $out = [];
        for ($i=0; $i < count($in)-1; $i++) { 
            if ($in[$i]['date']==$in[$i+1]['date']){
                $out[] = [
                    'date' => $in[$i]['date'], 
                    'summa' => $in[$i]['summa']+$in[$i+1]['summa'],
                    'summa_operator' => $in[$i]['summa_operator']+$in[$i+1]['summa_operator'],
                    'ostatok' => $in[$i]['ostatok']+$in[$i+1]['ostatok'],
                ];
                $i++;
            }else{
                $out[] = $in[$i];
            }

        }    
        if ((count($out)>0 && $out[count($out)-1]['date']!=$in[count($in)-1]['date']) ||
            (count($in)==1))
            $out[] = $in[count($in)-1];
        return $out;
    }

    public function actionDetailreport(){
        $allModels = [];
        $model = new ReportMonthForm();      
        if ($model->load(Yii::$app->request->post()) && $model->validate()){            
            $month = $model->month+1;
            if ($model->month<10) $model->month = '0'.$model->month;
            if ($month<10) $month = '0'.$month;
            $query = ['and'];
            $query[] = ['>', 'date', $model->year.'-'.$model->month.'-00']; 
            if ($model->month==12){
                $query[] = ['<', 'date', ($model->year+1).'-01-01'];                 
            }else{
                $query[] = ['<', 'date', $model->year.'-'.$month.'-00'];                 
            }                      
            $pays = Pay::find()
                ->where($query)
                ->orderBy('date')
                ->all(); 
            foreach ($pays as $pay) {             
                $summa_operator = $pay->summa;
                    if ($pay->fixed_summa>0)
                        $_summa = $pay->fixed_summa;
                    else
                        $_summa = $this->getGeneralSumma($pay->summa);
                    $ostatok = $_summa;
                $summa = $_summa+$summa_operator;
                    $allModels[] = [
                        'contract' => $pay->contract,
                        'customer' => $pay->customer,
                        'operator' => $pay->operator->title,
                        'date' => $pay->date,
                        'summa' => $summa,
                        'summa_operator' => $summa_operator,
                        'ostatok' => $ostatok,
                        'type' => 'Основной',
                    ];                           
            }

            $pays = AddPay::find()
                ->where($query)
                ->orderBy('date')
                ->all(); 
            foreach ($pays as $pay) {             
                $summa_operator = $pay->summa;
                    if ($pay->fixed_summa>0)
                        $_summa = $pay->fixed_summa;
                    else
                        $_summa = $this->getGeneralSumma($pay->summa);
                    $ostatok = $_summa;
                    $allModels[] = [
                        'contract' => $pay->pay->contract,
                        'customer' => $pay->pay->customer,
                        'operator' => $pay->pay->operator->title,
                        'date' => $pay->date,
                        'summa' => $summa,
                        'summa_operator' => $summa_operator,
                        'ostatok' => 0,
                        'type' => 'Дополнительный',
                    ];                           
            }
            $pays = BackPay::find()
                ->where($query)
                ->orderBy('date')
                ->all();       
            foreach ($pays as $pay) {                             
                    $allModels[] = [
                        'contract' => $pay->pay->contract,
                        'customer' => $pay->pay->customer,
                        'operator' => $pay->pay->operator->title,
                        'date' => $pay->date,
                        'summa' => $pay->summa_customer,
                        'summa_operator' => $pay->summa_operator,
                        'ostatok' => $pay->ostatok,
                        'type' => 'Возврат',
                    ];                           
            }
            usort($allModels, function ($val1, $val2){
                    if ($val1['date']<$val2['date']) return -1;
                    elseif ($val1['date']>$val2['date']) return 1;
                    else return 0;
            });  
            $model->month = (int)$model->month;
        }
        $query = new Query();
        $dataProvider = new ArrayDataProvider([
            'allModels' => $allModels,
            // 'sort' => [
            //     'attributes' => ['id', 'username', 'email'],
            // ],
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        return $this->render('detailReport', ['model' => $model, 'dataProvider' => $dataProvider]);                          
    }
}